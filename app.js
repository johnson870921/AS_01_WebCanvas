var cPushArray = new Array();
var cStep = -1;
var canvas;
var ctx;
var mode = 1;
var drag = false;
var brushSize = 4;
var canvasWidth, canvasHeight;
var lastPosX = 0 , lastPosY = 0;
var textBlock;
var fontSize;
var fontFace;
var color = "#000000";
var isFill = false;

window.onload = function(){
    canvas = document.getElementById("myCanvas");
    ctx = canvas.getContext("2d");
    canvas.addEventListener("mousemove", doMouseMove, false);
    canvas.addEventListener("mousedown", doMouseDown, false);
    canvas.addEventListener("mouseup", doMouseUp, false);
    textBlock = document.getElementById("textBlock");
    textBlock.addEventListener("keydown", inputConfirm, false);
    document.getElementById("brushSize").addEventListener("input", changeBrushSize, false);
    cPush();
}


function changeMode(idName){
        //console.log(idName);
        
}

function cPush() {
    cStep++;
    if(cStep < cPushArray.length){
        cPushArray.length = cStep;
    }
    cPushArray.push(ctx.getImageData(0,0,640,480));
    
}

function cUndo() {
    var canvas = document.getElementById("myCanvas");
    var ctx = canvas.getContext("2d");
    if(cStep > 0){
        cStep--;  
        ctx.putImageData(cPushArray[cStep], 0, 0);
    }
}

function cRedo() {
    var canvas = document.getElementById("myCanvas");
    var ctx = canvas.getContext("2d");
    if (cStep < cPushArray.length-1) {
        cStep++;
        ctx.putImageData(cPushArray[cStep], 0, 0);
    }
}

function resetCanvas(){
    var canvas = document.getElementById("myCanvas");
    var ctx = canvas.getContext("2d");
    ctx.clearRect(0, 0, canvas.width, canvas.height);
}

function changeMode(m){
    if(m == "btn1"){//brush
        mode = 1;
        canvas.style.cursor = 'url(img/pen.ico) 0 30, auto';
    }
    else if(m == "btn2"){//erase
        mode = 2;
        canvas.style.cursor = 'url(img/eraser.ico) 0 30, auto';
    }
    else if(m == "btn3"){//triangle
        mode = 3;
        canvas.style.cursor = 'url(img/magic.png) 0 30, auto';
    }
    else if(m == "btn4"){//circle
        mode = 4;
        canvas.style.cursor = 'url(img/magic.png) 0 30, auto';
    }
    else if(m == "btn5"){//rectangle
        mode = 5;
        canvas.style.cursor = 'url(img/magic.png) 0 30, auto';
    }
    else if(m == "btn6"){//line
        mode = 6;
        canvas.style.cursor = 'url(img/magic.png) 0 30, auto';
    }
    else if(m == "btn7"){//text input
        mode = 7;
        canvas.style.cursor = 'url(img/magic.png) 0 30, auto';
    }
}

function doMouseMove(event) {
    if(drag){
        canvas = document.getElementById("myCanvas");
        ctx = canvas.getContext("2d");
        //console.log(event.offsetX + " " + event.offsetY);
        ctx.lineWidth = brushSize;
        var mouseX = event.offsetX;
        var mouseY = event.offsetY;
        if(mode == 1){
            ctx.beginPath();
            ctx.moveTo(lastPosX, lastPosY);
            ctx.lineTo(mouseX, mouseY);
            lastPosX = mouseX;
            lastPosY = mouseY;
            ctx.stroke();
        }
        else if(mode == 2){
            ctx.beginPath();
            ctx.clearRect(mouseX, mouseY, brushSize, brushSize);

        }
        else if(mode == 4){
            ctx.putImageData(cPushArray[cStep],0,0);
            ctx.beginPath();
            ctx.arc(
                (lastPosX+mouseX)/2,
                (lastPosY+mouseY)/2,
                Math.hypot(Math.abs(lastPosX-mouseX)/2,
                Math.abs(lastPosY-mouseY)/2),
                0,
                2*Math.PI
                );
            if(isFill){
                ctx.fillStyle = color;
                ctx.fill();
            }
            ctx.stroke();
        }
        else if(mode == 3){
            ctx.putImageData(cPushArray[cStep],0,0);
            ctx.beginPath();
            ctx.moveTo(lastPosX, lastPosY);
            ctx.lineTo(mouseX, mouseY);
            ctx.lineTo(2*lastPosX-mouseX, mouseY);
            ctx.lineTo(lastPosX, lastPosY);
            if(isFill){
                ctx.fillStyle = color;
                ctx.fill();
            }
            ctx.stroke();
        }
        else if(mode == 5){
            ctx.putImageData(cPushArray[cStep],0,0);
            ctx.beginPath();
            ctx.moveTo(lastPosX, lastPosY);
            ctx.lineTo(mouseX, lastPosY);
            ctx.lineTo(mouseX, mouseY);
            ctx.lineTo(lastPosX, mouseY);
            ctx.lineTo(lastPosX, lastPosY);
            if(isFill){
                ctx.fillStyle = color;
                ctx.fill();
            }
            ctx.stroke();
        }
        else if(mode == 6){
            ctx.putImageData(cPushArray[cStep],0,0);
            ctx.beginPath();
            ctx.moveTo(lastPosX, lastPosY);
            ctx.lineTo(mouseX, mouseY);
            ctx.stroke();
        }
        else if(mode == 7){
            //text input mode
        }
    }
    
}

function doMouseDown(event) {
    drag = true;
    color = document.getElementById("hex-str").innerHTML;
    //console.log("color code: " + color);
    ctx.strokeStyle = color;
    ctx.fillStyle = color;

    lastPosX = event.offsetX;
    lastPosY = event.offsetY;
    if(mode == 7){
        textInput(lastPosX, lastPosY,color);
        
    }
}

function doMouseUp(event) {
    drag = false;
    if(mode != 7){//do not record when doing text input
        cPush();
    } 
    ctx.closePath();
}

function inputConfirm(event){
    if(event.keyCode == 13){
        ctx.font = fontSize + "px " + fontFace;
        ctx.fillText(textBlock.value, lastPosX-15, lastPosY);
        textBlock.value = null;
        cPush();
        textBlock.style.display = "none";
    }
}

function changeFontFace(){
    fontFace = document.getElementById("fontFace").value;
    //console.log(fontFace);
}

function changeFontSize(){
    fontSize = document.getElementById("fontSize").value;
    //console.log(fontSize);
}

function textInput(lastPosX, lastPosY){
    //console.log(lastPosX + " " + lastPosY);
    if(lastPosY < 440 && lastPosX < 510){
        textBlock.style.display = "block"; //set visiable
        textBlock.style.color = color;
        textBlock.value = null;
        textBlock.style.left = lastPosX + 15 + "px";
        textBlock.style.top = lastPosY + "px";
    }  
}

function changeBrushSize(event){
    brushSize = document.getElementById("brushSize").value;
    document.getElementById("showBrushSize").innerHTML = brushSize;
}

function doInput(id){
    var inputObj = document.createElement('input');
    inputObj.addEventListener('change',readFile,false);
    inputObj.type = 'file';
    inputObj.accept = 'image/*';
    inputObj.id = id;
    inputObj.click();
}

function readFile(){
    var file = this.files[0];
    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function(e){
    drawToCanvas(this.result);
    }
}

function drawToCanvas(imgData){
    var cvs = document.querySelector('#myCanvas');
    cvs.width=640;
    cvs.height=480;
    var ctx = cvs.getContext('2d');
    var img = new Image;
    img.src = imgData;
    img.onload = function(){
    ctx.drawImage(img,0,0,640,480);
    strDataURI = cvs.toDataURL();
    cPush();
    }
}

function downloadCanvas(obj){
    obj.href = canvas.toDataURL();

}

function toggleIsFilled(){
    obj = document.getElementById("toggleFilled");
    if(isFill){
        isFill = false;
        obj.alt = "Unfilled";
        obj.src = "img/unfilled.png";
    }
    else{
        isFill = true;
        obj.alt = "Filled";
        obj.src = "img/filled.png";
    }
}