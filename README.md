# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | Y         |


---

### How to use 
    特色:
    1.首先背景的部分在CSS使用linear-gradient來實作漸層效果。
    2.使用Bootstrap Grid來編排位置，使網頁有Respond Wed Design(RWD)的效果。
    3.所有Button在點擊時都會在點擊的地方產生一個半透明的圓圈，利用CSS中animate
    來實作圓圈半徑由小到大增加，使點擊產生漣漪的效果。
    4.根據目前使用的模式來改變Cursor在Canvas中的樣式。
    
    功能:
![](https://i.imgur.com/4byLyOe.jpg)
![](https://i.imgur.com/5lZOH5U.png)

    最上面的調色盤只要點擊框框內的區域，就會有調色盤可供選擇顏色，並將選擇到的
    顏色的對應Color code顯示到框框中。這邊使用的相關函式由jscolor.js來實現。
![](https://i.imgur.com/UwoXtt6.png)
    
    接下來的畫筆icon利用html中的range slider來實現利用拉霸來調整畫筆粗細，最粗
    為20，最細為4，預設值為4。
![](https://i.imgur.com/RGA4twU.png)

    接下來的區域利用下拉式選單可以調整字型與字體大小，字型有Monospace, Fantasy,
    Cursive, Serif等四種html內建字型。字體大小則有12, 16, 20, 24等四種不同大小
    可以選擇。
![](https://i.imgur.com/367ur74.png)
    
    按鈕按下去的時候，利用onchange來呼叫js中對應的function來改變當前使用模式。
    實作方法:
    1.PEN: 當在Canvas內進行拖曳的時候，每次更新畫面都會劃出一條線，座標為由前一次
    畫面中Cursor的位置到這次畫面Cursor的位置，來進行實作。
    
    2.ERASER: 當在Canvas內進行拖曳的時候，會使用clearRect()來清空長方形區域內的
    所有東西，座標為當前Curosr位置與設定的Brush size。
    
    3.TRIANGLE: 當在Canvas內進行拖曳的時候，會先把畫面還原回還沒開始畫三角形的
    Canvas，接著根據開始拖曳時起始的Cursor位置與當前Cursor位置畫出一個等腰三角形。
    
    4.Circle: 當在Canvas內進行拖曳的時候，會先把畫面還原回還沒開始畫圓形的Canvas，
    接著根據開始拖曳時起始的Cursor位置與當前Cursor位置畫出一個圓形。
    
    5.RECTANGLE: 當在Canvas內進行拖曳的時候，會先把畫面還原回還沒開始畫長方形的
    Canvas，接著根據開始拖曳時起始的Cursor位置與當前Cursor位置畫出一個長方形。
    
    6.LINE: 當在Canvas內進行拖曳的時候，會先把畫面還原回還沒開始畫直線的Canvas，
    接著根據開始拖曳時起始的Cursor位置與當前Cursor位置畫出一個直線。
![](https://i.imgur.com/df6WWqq.png)

    按下TEXT後，當在Canvas點擊時，原本在html中隱藏的input區域會移動到點擊時的
    Cursor位置，並顯示出來。當打完所有input內容的時候，案下enter會觸發一開始註冊
    的keyboard eventlistener，去callback js中的inputConfirm(event)，藉由
    event.keyCode偵測是否為案下enter，並將input區域內的value由fillText()來
    顯示到Canvas上。
![](https://i.imgur.com/vPB6ZQG.png)
    
    按下RESET後，callback js中的resetCanvas()，使用與ERASER中相同的
    clearRect() function來清除整個Canvas大小的長方形區域。
![](https://i.imgur.com/q1SOXQ5.png)

    每次在Canvas中，mouseup的時候都會觸doMouseUp(event)，其中會呼叫cPush()
    來將現在Canvas中的內容放入cPushArray中儲存。
    1.REDO: 會將cPushArray中的下一個內容放入Canvas中。
    (RESET後還是可以REDO的)
    2.UNDO: 會將cPushArray中的前一個內容放入Canvas中。
![](https://i.imgur.com/CLfYTXq.png)

    在DOWNLOAD的tag中，download attriuate會將Canvas存成canvas.png。其中
    download的來源由href決定，所以在callback function downloadCanvas()中
    將Canvas使用toDataURL來指定給href。



    

### Function description
    
    實作出的Bonus功能有兩個，分別為上傳圖片與調整畫出的圖形是否為填滿色彩。
![](https://i.imgur.com/kfkplE0.png)
    
    按下UPLOAD時會出現彈出式視窗來選擇本地圖檔，當選擇到東西的時候觸發
    event listener來執行readFile()以讀取選擇到的第一個檔案，並會再呼叫
    drawToCanvas()將圖片顯示到Canvas上。
![](https://i.imgur.com/mM9pRIZ.png)
![](https://i.imgur.com/WEdjPwm.png)
    
    點擊最下面的正方形圖案時，會將之後拖曳出的圖形變成塗滿的樣式，且正方型圖案也會
    變成塗滿的樣子。反之亦然，再點擊一次的時候，會變回原本的狀態。
    
    


### Gitlab page link
    
    https://johnson870921.gitlab.io/AS_01_WebCanvas

### Others (Optional)

    這份作業中用到之前上課沒教到的東西真的太多了，雖然花了不少時間來debug，而且
    一開始決定要用Bootstrap Grid也造成Canvas內部座標判斷的麻煩，但是看到最後
    完成的作業成就感還是很大的。果然從一項project中能學到的東西還是比上課中學到
    的東西來的多，最後辛苦助教大大了。

<style>
table th{
    width: 100%;
}
</style>